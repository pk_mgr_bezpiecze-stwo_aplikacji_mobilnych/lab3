package com.mj.lab3;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    final int READ_CONTACTS_PERMISSION_REQUEST_CODE = 152222;

    //Lab3_1_3
    private ConnectivityManager connectivityManager;
    //Lab3_1_3
    private BroadcastReceiver networkReceiver;

    //Lab3_1_3
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Lab3_1_1
        final Button execButton = findViewById(R.id.execBtn);

        //Lab3_1_2
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    URL url = new URL("https://jsonplaceholder.typicode.com/posts");
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("Content-Type", "application/json");

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuilder content = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    in.close();
                    Log.v("Raw response", content.toString());

                    JsonArray convertedObjects = new Gson().fromJson(content.toString(), JsonArray.class);

                    for (int i = 0; i < convertedObjects.size(); i++) {
                        Log.i("JSON-object" ,"Position: " +i + ": "+ convertedObjects.get(i).getAsJsonObject().toString());
                    }

                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        execButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("state", thread.getState().toString());
                if(thread.getState()== Thread.State.NEW){
                    thread.start();
                }
            }
        });


        //Lab3_1_3
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        Log.d("Lab3_1_3", "Is connected: "+ networkInfo.isConnected());
        Log.d("Lab3_1_3", "Type: "+ networkInfo.getTypeName() +" - "+ ConnectivityManager.TYPE_WIFI);


        networkReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if(networkInfo != null){
                    Log.d("Lab3_1_3", "Is connected: "+ networkInfo.isConnected());
                    Log.d("Lab3_1_3", "Type: "+ networkInfo.getTypeName() +" - "+ ConnectivityManager.TYPE_WIFI);
                } else {
                    Log.d("Lab3_1_3", "Network info is null, probably network is turned off");
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkReceiver, intentFilter);

        //Lab3_2_1
        final Button contactButton = findViewById(R.id.readButton);

        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getContacts();
            }
        });

        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS}, READ_CONTACTS_PERMISSION_REQUEST_CODE);
    }

    //Lab3_2_1
    private void getContacts() {
        Cursor cursorWithContacts = getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
                );

        if (cursorWithContacts != null) {
            if (cursorWithContacts.moveToFirst()) {
                do {
                    if(cursorWithContacts.getColumnIndex(ContactsContract.Contacts._ID)>=0){
                        @SuppressLint("Range") String contactId = cursorWithContacts.getString(cursorWithContacts.getColumnIndex(ContactsContract.Contacts._ID));
                        @SuppressLint("Range") String displayName =
                                cursorWithContacts.getString(cursorWithContacts.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        Log.d("Lab3_2_1", "Contact: "+ contactId+", "+displayName);
                    }
                } while (cursorWithContacts.moveToNext());
            }
            cursorWithContacts.close();
        }
    }

    //Lab3_2_1
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("Lab3_2_1", "Checking contact permission");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_CONTACTS_PERMISSION_REQUEST_CODE) {
            if (grantResults.length>0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                getContacts();
            } else {
                Toast.makeText(getApplicationContext(), "You don't have permission.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}